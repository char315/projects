//create your .env file in the same directory 
const dotenv = require('dotenv');
dotenv.config();

module.exports = {
  db_url: process.env.DB_URL, // The local MongoDB URL
  port: process.env.PORT, // The localhost port, typically 8080
  secretKey: process.env.SECRET_KEY, // Secret key for accessing server
  emailprvdr: process.env.EMAIL_PRVDR, // Email provider for password reset
  emailuname: process.env.EMAIL_UNAME, // Email username
  emailpwd: process.env.EMAIL_PWD // Email password
};