var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const plotTestSchema = new Schema({
    userID: String,
    name: String,
    storyArcs: Number,
    world: String,
    year: String,
    goal: String
});

var PlotTest = mongoose.model('testplot',plotTestSchema);

module.exports = PlotTest;

