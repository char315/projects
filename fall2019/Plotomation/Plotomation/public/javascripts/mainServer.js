// Deletes a selected row from a given table
function deleteRow(rowIndex, id) {
    var currTable = id.substring(6)
    document.getElementById(currTable).deleteRow(rowIndex)
}

// Adds to an array of Promises if a given text value is empty
function promiseCheck(sentText, sentBtn, currArray) {
    if(document.getElementById(sentText).value == "") {
        var promise = new Promise(function(resolve) {
            getRandomAttribute(sentBtn, resolve)
        })
        currArray.push(promise)
    }
    return currArray
}

// Adds a row to a givvne table and fills out the relevant information
function addRow(currTable, btnGroupResult) {
    var table = document.getElementById(currTable).getElementsByTagName('tbody')[0]
    var newRow = table.insertRow()

    if(currTable == 'characterTable') {
        // Create the row
        var newCell = newRow.insertCell(0); newCell.contentEditable = true; newCell.appendChild(document.createTextNode(document.getElementById('cNametxt').value))
        newCell = newRow.insertCell(1); newCell.contentEditable = true; newCell.appendChild(document.createTextNode(btnGroupResult))
        newCell = newRow.insertCell(2); newCell.contentEditable = true; newCell.appendChild(document.createTextNode(document.getElementById('cDescriptiontxt').value))
        newCell = newRow.insertCell(3); newCell.contentEditable = true; newCell.appendChild(document.createTextNode(document.getElementById('cRacetxt').value))
        newCell = newRow.insertCell(4); newCell.contentEditable = true; newCell.appendChild(document.createTextNode(document.getElementById('cClasstxt').value))
        newCell = newRow.insertCell(5); newCell.contentEditable = true; newCell.appendChild(document.createTextNode(document.getElementById('cPersonalitytxt').value))
        newCell = newRow.insertCell(6); newCell.contentEditable = true; newCell.appendChild(document.createTextNode(document.getElementById('cInfo').value))
        newCell = newRow.insertCell(7); newCell.innerHTML = "<button type=\"button\" class=\"btn btn-danger btn-rounded btn-sm my-0\" id=\"btndltcharacterTable\" onclick=\"deleteRow($(this).closest('td').parent()[0].sectionRowIndex+1, this.id);\">Remove</button>"

        // Reset character fields
        document.getElementById('cNametxt').value = ''
        var btnContainer = document.getElementById('gender')
        var current = btnContainer.getElementsByClassName('active')
        if(current[0] != null) {
            current[0].className = current[0].className.replace(" active", "")
        }
        document.getElementById('cDescriptiontxt').value = ''
        document.getElementById('cRacetxt').value = ''
        document.getElementById('cClasstxt').value = ''
        document.getElementById('cPersonalitytxt').value = ''
        document.getElementById('cInfo').value = ''
    }
    if(currTable == 'questTable') {
        // Create the row
        var newCell = newRow.insertCell(0); newCell.contentEditable = true; newCell.appendChild(document.createTextNode(document.getElementById('qGoaltxt').value))
        newCell = newRow.insertCell(1); newCell.contentEditable = true; newCell.appendChild(document.createTextNode(document.getElementById('qRewardtxt').value))
        newCell = newRow.insertCell(2); newCell.contentEditable = true; newCell.appendChild(document.createTextNode(document.getElementById('qAssigneetxt').value))
        newCell = newRow.insertCell(3); newCell.contentEditable = true; newCell.appendChild(document.createTextNode(document.getElementById('qInfo').value))
        newCell = newRow.insertCell(4); newCell.innerHTML = "<button type=\"button\" class=\"btn btn-danger btn-rounded btn-sm my-0\" id=\"btndltquestTable\" onclick=\"deleteRow($(this).closest('td').parent()[0].sectionRowIndex+1, this.id);\">Remove</button>"

        // Reset quest fields
        document.getElementById('qGoaltxt').value = ''
        document.getElementById('qRewardtxt').value = ''
        document.getElementById('qAssigneetxt').value = ''
        document.getElementById('qInfo').value = ''
    }
    if(currTable == 'locationTable') {
        // Create the row
        var newCell = newRow.insertCell(0); newCell.contentEditable = true; newCell.appendChild(document.createTextNode(btnGroupResult))
        newCell = newRow.insertCell(1); newCell.contentEditable = true; newCell.appendChild(document.createTextNode(document.getElementById('lNametxt').value))
        newCell = newRow.insertCell(2); newCell.contentEditable = true; newCell.appendChild(document.createTextNode(document.getElementById('lDescriptiontxt').value))
        newCell = newRow.insertCell(3); newCell.contentEditable = true; newCell.appendChild(document.createTextNode(document.getElementById('lCountryNametxt').value))
        newCell = newRow.insertCell(4); newCell.contentEditable = true; newCell.appendChild(document.createTextNode(document.getElementById('lOwnertxt').value))
        newCell = newRow.insertCell(5); newCell.contentEditable = true; newCell.appendChild(document.createTextNode(document.getElementById('lInfo').value))
        newCell = newRow.insertCell(6); newCell.innerHTML = "<button type=\"button\" class=\"btn btn-danger btn-rounded btn-sm my-0\" id=\"btndltlocationTable\" onclick=\"deleteRow($(this).closest('td').parent()[0].sectionRowIndex+1, this.id);\">Remove</button>"

        // Reset location fields
        var btnContainer = document.getElementById('location')
        var current = btnContainer.getElementsByClassName('active')
        if(current[0] != null) {
            current[0].className = current[0].className.replace(" active", "")
        }
        document.getElementById('lNametxt').value = ''
        document.getElementById('lDescriptiontxt').value = ''
        document.getElementById('lCountryNametxt').value = ''
        document.getElementById('lOwnertxt').value = ''
        document.getElementById('lInfo').value = ''
    }
    if(currTable == 'encounterTable') {
        // Create the row
        var newCell = newRow.insertCell(0); newCell.contentEditable = true; newCell.appendChild(document.createTextNode(btnGroupResult))
        newCell = newRow.insertCell(1); newCell.contentEditable = true; newCell.appendChild(document.createTextNode(document.getElementById('eNametxt').value))
        newCell = newRow.insertCell(2); newCell.contentEditable = true; newCell.appendChild(document.createTextNode(document.getElementById('eDifficultytxt').value))
        newCell = newRow.insertCell(3); newCell.contentEditable = true; newCell.appendChild(document.createTextNode(document.getElementById('eLocationtxt').value))
        newCell = newRow.insertCell(4); newCell.contentEditable = true; newCell.appendChild(document.createTextNode(document.getElementById('eInfo').value))
        newCell = newRow.insertCell(5); newCell.innerHTML = "<button type=\"button\" class=\"btn btn-danger btn-rounded btn-sm my-0\" id=\"btndltencounterTable\" onclick=\"deleteRow($(this).closest('td').parent()[0].sectionRowIndex+1, this.id);\">Remove</button>"

        // Reset location fields
        var btnContainer = document.getElementById('eType')
        var current = btnContainer.getElementsByClassName('active')
        if(current[0] != null) {
            current[0].className = current[0].className.replace(" active", "")
        }
        document.getElementById('eDifficultytxt').value = ''
        document.getElementById('eNametxt').value = ''
        document.getElementById('eLocationtxt').value = ''
        document.getElementById('eInfo').value = ''
    }
}

// Randomizes any blank input, regardless if a new row is being added to a table or not
function fillInBlankInput(id, resolve) {
    var currTableStr = id.substring(6)
    var arrayOfChecks = []
    var btnResult = ''

    // Row being added to Character table
    if(currTableStr == 'characterTable') {
        // Randomize elements if empty
        if(!(document.getElementById('cCheck').checked)) {
            // Character info
            fillInButtonGroup('gender', 3)
            arrayOfChecks = promiseCheck('cNametxt', 'cName', arrayOfChecks)
            arrayOfChecks = promiseCheck('cDescriptiontxt', 'cDescription', arrayOfChecks)
            arrayOfChecks = promiseCheck('cRacetxt', 'cRace', arrayOfChecks)
            arrayOfChecks = promiseCheck('cClasstxt', 'cClass', arrayOfChecks)
            arrayOfChecks = promiseCheck('cPersonalitytxt', 'cPersonality', arrayOfChecks)

            btnResult = returnButtonInGroup('gender')
        }
        Promise.all(arrayOfChecks).then(function(value) {
            addRow('characterTable', btnResult)
            if(resolve)
                resolve(value)
        });
    }

    // Row being added to Quest table
    if(currTableStr == 'questTable') {
        // Randomize elements if empty
        if(!(document.getElementById('qCheck').checked)) {
            // Quest info
            arrayOfChecks = promiseCheck('qGoaltxt', 'qGoal', arrayOfChecks)
            arrayOfChecks = promiseCheck('qRewardtxt', 'qReward', arrayOfChecks)
            arrayOfChecks = promiseCheck('qAssigneetxt', 'qAssignee', arrayOfChecks)
        }

        Promise.all(arrayOfChecks).then(function(value) {
            addRow('questTable')
            if(resolve)
                resolve(value)
        });
    }

    // Row being added to Location table
    if(currTableStr == 'locationTable') {
        // Randomize elements if empty
        if(!(document.getElementById('lCheck').checked)) {
            // Location info
            fillInButtonGroup('location', 4)
            arrayOfChecks = promiseCheck('lNametxt', 'lName', arrayOfChecks)
            arrayOfChecks = promiseCheck('lDescriptiontxt', 'lDescription', arrayOfChecks)
            btnResult = returnButtonInGroup('location')

            // If no row is on the table yet, the first location must be a country
            if($('#locationTable tr').length < 2) {
                btnResult = 'Country'
            }

            if((document.getElementById('lCountryNametxt').value == "") && (btnResult != 'Country')) {
                document.getElementById('lCountryCheck').checked = true
                document.getElementById('lCountryNametxt').disabled = false

                var locationTable = document.getElementById('locationTable')

                // Automatically make a blank place of origin for city the first country on table
                if(btnResult == 'City' || btnResult == 'General') {
                    document.getElementById('lCountryNametxt').value = locationTable.rows[1].cells[1].innerHTML
                } else { // Automatically make a blank place of origin for shop a city if possible, otherwise remains blank
                    for (var r = 1, n = locationTable.rows.length; r < n; r++) {
                        for (var c = 0, m = locationTable.rows[r].cells.length; c < m - 1; c++) {
                            if(locationTable.rows[r].cells[c].innerHTML == 'City') {
                                document.getElementById('lCountryNametxt').value = locationTable.rows[r].cells[c+1].innerHTML
                            }
                        }
                    }   
                }
            }   
            arrayOfChecks = promiseCheck('lOwnertxt', 'lOwner', arrayOfChecks)  
        }

        Promise.all(arrayOfChecks).then(function(value) {
            addRow('locationTable', btnResult)
            if(resolve)
                resolve(value)
        });
    }

    // Row being added to Encounter table
    if(currTableStr == 'encounterTable') {
        // Randomize elements if empty
        if(!(document.getElementById('eCheck').checked)) {
            // Encounter info
            fillInButtonGroup('eType', 2)
            arrayOfChecks = promiseCheck('eNametxt', 'eName', arrayOfChecks)
            arrayOfChecks = promiseCheck('eDifficultytxt', 'eDifficulty', arrayOfChecks)

            var locationTable = document.getElementById('locationTable')

            // If an encounter's location is blank, attempts to randomize its location to being the first instance
            // of a General location elsewhere on the table; otherwise stays blank
            if(document.getElementById('eLocationtxt').value == "") {
                for (var r = 1, n = locationTable.rows.length; r < n; r++) {
                    for (var c = 0, m = locationTable.rows[r].cells.length; c < m - 1; c++) {
                        if(locationTable.rows[r].cells[c].innerHTML == 'General') {
                            document.getElementById('eLocationtxt').value = locationTable.rows[r].cells[c+1].innerHTML
                        }
                    }
                } 
            }  

            btnResult = returnButtonInGroup('eType')
        }

        Promise.all(arrayOfChecks).then(function(value) {
            addRow('encounterTable', btnResult)
            if(resolve)
                resolve(value)
        });
    }
}

// A different story arc button has been selected
document.getElementById('arc').onclick = function() {
    changeActiveBtn('arc')
}

// A different gender button has been selected
document.getElementById('gender').onclick = function() {
    changeActiveBtn('gender')
}

// A different location type button has been selected
document.getElementById('location').onclick = function() {
    changeActiveBtn('location')
}

// A different encounter type button has been selected
document.getElementById('eType').onclick = function() {
    changeActiveBtn('eType')
}

// Change active button from a selected Button Group
function changeActiveBtn(btnGroup) {
    // Get the container element
    var btnContainer = document.getElementById(btnGroup)

    // Get all buttons with class="btn" inside the container
    var btns = btnContainer.getElementsByClassName('btn btn-secondary')

    // Loop through the buttons and add the active class to the current/clicked button
    for (var i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", function() {
            var current = btnContainer.getElementsByClassName("active");
        
            // If there's no active class
            if (current.length > 0) {
                current[0].className = current[0].className.replace(" active", "");
            }
        
            // Add the active class to the current/clicked button
            this.className += " active";
        });
    }
}

// If logout tab in NavBar clicked, logs user out back to first page
document.getElementById('logout').onclick = function() {
    let port = window.location.port;
    window.location = "http://localhost:"+port+"/logout";
}

//ChangePassword functionality
document.getElementById('changePass').onclick = function() {
    window.location = "/changePwd";
}

// Enable Country Name field and button
document.getElementById('lCountryCheck').onclick = function() {
    if (!(document.getElementById('lCountryCheck').checked)) {
        document.getElementById('lCountryNametxt').disabled = true
    } else {
        document.getElementById('lCountryNametxt').disabled = false
    }  
}

// Gets random attribute from MongoDB, with the call higher above in this file
function getRandomAttribute(button, resolve) {
    var attribute = button.replace('btn', '');

    // Update relevant value on main UI
    var textToUpdate = attribute + 'txt'
    var result;
    if(attribute == 'tYear') {
        var rand = Math.floor(Math.random() * 1500);
        result = rand.toString()      
        document.getElementById(textToUpdate).value = result
        if(resolve) {
            resolve(result)
        }
    } else if(attribute == 'eDifficulty') {
        var rand = Math.floor(Math.random() * 30) + 1;
        result = rand.toString()
        document.getElementById(textToUpdate).value = result
        if(resolve) {
            resolve(result)
        }
    } else {
        // Updates attribute for back end call appropriately if a location description
        if(attribute == 'lDescription') {
            try {
                var btnResult = returnButtonInGroup('location')
                switch(btnResult) {
                    case 'Country':
                        attribute = 'lDescriptionCO'
                        break;
                    case 'City':
                        attribute = 'lDescriptionCI'
                        break;
                    case 'Shop':
                        attribute = 'lDescriptionS'
                        break;
                    case 'General':
                        attribute = 'lDescriptionG'
                        break;
                }              
            } catch(e) { attribute = 'lDescriptionG' }
        }
        // MongoDB request
        $.ajax( {
            type: "GET",
            url: '/api/random',
            data: attribute,
            success: (result) => {
                document.getElementById(textToUpdate).value = result
                if(resolve) {
                    resolve(result)
                }
            }
        })
    }
}

// Retrieve active button in Button Group
function returnButtonInGroup(btnGroup) { 
    var btnContainer = document.getElementById(btnGroup)
    var current = btnContainer.getElementsByClassName('active')
    return current[0].value
}

// Attempt to randomly choose button from a Button Group without an active button
function fillInButtonGroup(btnGroup, randNum) {
    var btnContainer = document.getElementById(btnGroup)
    var btns = btnContainer.getElementsByClassName('btn btn-secondary')
    var current = btnContainer.getElementsByClassName('active')
    if(current.length <= 0) {
        var rand = Math.floor(Math.random() * randNum);
        btns[rand].className += " active";
    }
}

// Randomize remaining empty elements, as well as ensure that there is
// at least one row in each of Character, Quest, Location and Encounter
function verifyForm(resolve) {
    var arrayOfChecks = []
    var finalValueList = []
    var promise1, promise2, promise3, promise4

    // Story title and arc # info
    fillInButtonGroup('arc', 5)
    arrayOfChecks = promiseCheck('sTitletxt', 'sTitle', arrayOfChecks)

    // Setting/theme/tone info
    if(!(document.getElementById('tCheck').checked)) {
        arrayOfChecks = promiseCheck('tOverGoaltxt', 'tOverGoal', arrayOfChecks)
        arrayOfChecks = promiseCheck('tLaconictxt', 'tLaconic', arrayOfChecks)
        arrayOfChecks = promiseCheck('tWorldtxt', 'tWorld', arrayOfChecks)
        arrayOfChecks = promiseCheck('tYeartxt', 'tYear', arrayOfChecks)
    }

    // Check table size of Character/Quest/Location/Encounter and add one row if empty
    if($('#characterTable tr').length == 1) {
        promise1 = new Promise(function(resolve) {
            fillInBlankInput('btnaddcharacterTable', resolve)
        });
    }
    if($('#questTable tr').length == 1) {
        promise2 = new Promise(function(resolve) {
            fillInBlankInput('btnaddquestTable', resolve)
        });
    }
    if($('#locationTable tr').length == 1) {
        promise3 = new Promise(function(resolve) {
            fillInBlankInput('btnaddlocationTable', resolve)
        });
    }
    if($('#encounterTable tr').length == 1) {
        promise4 = new Promise(function(resolve) {
            fillInBlankInput('btnaddencounterTable', resolve)
        });
    }

    // Ensure that all tables have at least one row before resolving Promises
    Promise.all([promise1, promise2, promise3, promise4]).then(function(firstValues) {
        finalValueList.push(firstValues)

        Promise.all(arrayOfChecks).then(function(secondValues) {
            firstValues.push(secondValues)
            resolve(firstValues)
        }); 
    });
}

// Loop through and add location data to JSON object
function locationSubmission() {
    var data = '"countries":['   
    var locationTermsList = ['"type":', '"name":', '"description":', '"origin":', '"owner":', '"notes":']
    var encounterTermsList = ['"name":', '"description":', '"difficulty":', '"location":', '"notes":']

    var encounterList = []
    var countryList = []
    var cityList = []
    var shopList = []
    var generalList = []

    var table = document.getElementById('encounterTable')
    // Append each encounter to its own array
    for (var r = 1, n = table.rows.length; r < n; r++) {
        var encounterData = '{\n'
        for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
            encounterData += encounterTermsList[c] + "\"" + table.rows[r].cells[c].innerHTML + '"'
            if(encounterTermsList[c] != '"notes":') {
                encounterData += ',\n'
            }
        }
        encounterData += '\n}'
        encounterData = JSON.parse(encounterData)
        encounterList.push(encounterData)
    }

    table = document.getElementById('locationTable')
    // Append countries, cities, shops and general each to their own array
    for (var r = 1, n = table.rows.length; r < n; r++) {
        var locationData = '{\n'
        for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
            // Append countries, cities, shops and locations
            locationData += locationTermsList[c] + "\"" + table.rows[r].cells[c].innerHTML + '"'
            if(locationTermsList[c] != '"notes":') {
                locationData += ',\n'
            }
        }
        // Attach current location data to the respective array after parsing it
        locationData += '\n}'
        locationData = JSON.parse(locationData)
        if(table.rows[r].cells[0].innerHTML == 'Country') { countryList.push(locationData) }
        if(table.rows[r].cells[0].innerHTML == 'City') { cityList.push(locationData) }
        if(table.rows[r].cells[0].innerHTML == 'Shop') { shopList.push(locationData) }
        if(table.rows[r].cells[0].innerHTML == 'General') { generalList.push(locationData) }
    }

    // Handle all countries
    for(var i = 0; i < countryList.length; i++) {
        var currCountry = countryList[i]

        data += '{\n"type":"' + countryList[i].type + '",\n"name":"' + countryList[i].name + '",\n"description":"' + countryList[i].description
            + '",\n"origin":"' + countryList[i].origin + '",\n"owner":"' + countryList[i].owner + '",\n"notes:":"' + countryList[i].notes + '"'

        // Handle all cities
        data += ',\n"cities":[{\n'
        var cityCount = 0
        for(var j = 0; j < cityList.length; j++) {
            var currCity = cityList[j]

            // Checks if there is a match between a given city and country or if the city's origin is empty
            if(currCity.origin == currCountry.name) {
                cityCount++ 
                if(cityCount > 1) {
                    data += '\n},{\n'
                }

                data += '"type":"' + cityList[j].type + '",\n"name":"' + cityList[j].name + '",\n"description":"'
                    + cityList[j].description + '",\n"origin":"' + cityList[j].origin + '",\n"owner":"' + cityList[j].owner 
                    + '",\n"notes":"' + cityList[j].notes + '"'

                data += ',\n"shops":[{\n'
                var shopCount = 0
                // Handle all shops
                for(var l = 0; l < shopList.length; l++) {
                    var currShop = shopList[l]
                    // Checks if there is a match between a given shop and city or if the shop's origin is empty
                    if(currShop.origin == currCity.name) {
                        shopCount++ 
                        if(shopCount > 1) {
                            data += '\n},{\n'
                        }
        
                        data += '"type":"' + shopList[l].type + '",\n"name":"' + shopList[l].name + '",\n"description":"'
                            + shopList[l].description + '",\n"difficulty":"' + shopList[l].origin + '",\n"owner":"' + shopList[l].owner 
                            + '",\n"notes":"' + shopList[l].notes + '"'
                    }
                }

                data += '\n}]'
            }
        }
        data += '\n}]'

        data += ',\n"locations":[{\n'
        var locationCount = 0
        // Handle all general locations
        for(var k = 0; k < generalList.length; k++) {
            var currLocation = generalList[k]
            // Checks if there is a match between a given location and country or if the location's origin is empty
            if(currLocation.origin == currCountry.name) {
                locationCount++ 
                if(locationCount > 1) {
                    data += '\n},{\n'
                }

                data += '"type":"' + generalList[k].type + '",\n"name":"' + generalList[k].name + '",\n"description":"'
                    + generalList[k].description + '",\n"origin":"' + generalList[k].origin + '",\n"owner":"' + generalList[k].owner 
                    + '",\n"notes":"' + generalList[k].notes + '"'

                data += ',\n"encounters":[{\n'
                var encounterCount = 0
                // Handle all encounters
                for(var m = 0; m < encounterList.length; m++) {
                    var currEncounter = encounterList[m]
                    // Checks if there is a match between a given encounter and location or if the encounter's origin is empty
                    if(currEncounter.location == currLocation.name) {
                        encounterCount++ 
                        if(encounterCount > 1) {
                            data += '\n},{\n'
                        }
        
                        data += '"name":"' + encounterList[m].name + '",\n"description":"' + encounterList[m].description + '",\n"difficulty":"'
                            + encounterList[m].difficulty + '",\n"location":"' + encounterList[m].location + '",\n"notes":"' + encounterList[m].notes + '"'
                    }
                }
                data += '\n}]'
            }
        }
        data += '\n}]\n}'
        if(i != countryList.length - 1) {
            data += ','       
        }
    }

    data += '],\n'
    return data
}

// Fetches username for logged user.
function get_username()
{
      //Fetches the username
      let uname = String(document.getElementById('uname').innerHTML)
      uname = uname.slice(13,uname.length-1);
      return '"' + uname + '"';
}

// Submit main UI form information to MongoDB 
function submit() {
    var promise = new Promise(function(resolve) {
        verifyForm(resolve)
    });

    var sentData = null;

    promise.then(function() {
        // Loop through initial/setting/theme/tone data and add to JSON object
        var finalData = '{\n"plot":{\n"userID":' + get_username() + ',\n'
        finalData += '"name":"' + document.getElementById('sTitletxt').value + '",\n'
        finalData += '"storyArcs":"' + returnButtonInGroup('arc') + '",\n'
        finalData += '"world":"' + document.getElementById('tWorldtxt').value + '",\n'
        finalData += '"year":"' + document.getElementById('tYeartxt').value + '",\n'
        finalData += '"goal":"' + document.getElementById('tOverGoaltxt').value + '",\n'
        finalData += '"laconic":"' + document.getElementById('tLaconictxt').value + '",\n'

        // Loop through location/encounter tables and add all locations/encounters to JSON object
        var locationData = locationSubmission()
        finalData += locationData 

        // Loop through quest table and add all quests to JSON object
        var questData = '"quests":[{\n'
        termsList = ['"goal":', '"reward":', '"assignee":', '"notes":']
        table = document.getElementById('questTable')
        for (var r = 1, n = table.rows.length; r < n; r++) {
            for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
                questData += termsList[c] + "\"" + table.rows[r].cells[c].innerHTML + '"'
                if(termsList[c] == '"notes":' && r < n - 1) {
                    questData += '\n},{\n' 
                }
                if(termsList[c] != '"notes":') {
                    questData += ',\n'
                }
            }
        }
        questData += '\n}],\n'
        finalData += questData 

        // Loop through character table and add all characters to JSON object
        var characterData = '"characters":[{\n'
        var termsList = ['"name":', '"gender":', '"description":', '"race":', '"class":', '"personality":', '"notes":']
        var table = document.getElementById('characterTable')
        for (var r = 1, n = table.rows.length; r < n; r++) {
            for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
                characterData += termsList[c] + "\"" + table.rows[r].cells[c].innerHTML + '"'
                if(termsList[c] == '"notes":' && r < n - 1) {
                    characterData += '\n},{\n' 
                }
                if(termsList[c] != '"notes":') {
                    characterData += ',\n'
                }
            }
        }
        characterData += '\n}]\n}\n}'
        finalData += characterData

        // Confirming that the JSON string correctly parses into an object before sending
        console.log(finalData)
        finalData = finalData.replace(/[\/\\]/g, '');
        finalData = finalData.replace(/<br>/g, '');
        finalData = JSON.parse(finalData)
        sentData = JSON.stringify(finalData)
        console.log(sentData)
        
    }).finally( function(){
            // Submit post request using a stringified JSON object
            $.ajax( {
                type: "POST",
                url: '/api/submit',
                dataType: 'application/json',
                data: sentData,
                success: (result) => {
                }
            })
        });
    setTimeout(function() {window.location = "/plot"},4000);
}