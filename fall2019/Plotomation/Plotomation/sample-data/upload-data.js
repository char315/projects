require('dotenv').config()
const MongoClient = require('mongodb').MongoClient

const url = process.env.MONGO_CONNECTION

const client = new MongoClient(url, {useUnifiedTopology: true})


