const mongoose = require('mongoose'),
    Schema = mongoose.Schema

const themesSchema = new Schema({
    type: String,
    description: String,
    notes: String
})

const termsSchema = new Schema({
    name: String,
    type: [String]
})

const ThemesModel = mongoose.model('Theme', themesSchema)
const TermsModel = mongoose.model('Term', termsSchema)

module.exports = {
    Theme: ThemesModel,
    Term: TermsModel
}