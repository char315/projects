const mongoose = require('mongoose'),
    Schema = mongoose.Schema
    mongoose.set('useFindAndModify', false)

const encounterSchema = new Schema({
    name: Schema.Types.Mixed,
    type: String,
    difficulty: String,
    notes: String
})

const countrySchema = new Schema({
    name: String,
    description: String,
    owner: String,
    cities: [{type: Schema.Types.ObjectId, ref: 'City'}],
    locations: [{type: Schema.Types.ObjectId, ref: 'Location'}],
    notes: String
})

const shopSchema = new Schema({
    name: String,
    owner: [{type: Schema.Types.Mixed}],
    type: String,
    notes: String
})

const citySchema = new Schema({
    name: String,
    description: String,
    owner: String,
    shops: [{type: Schema.Types.ObjectId, ref: 'Shop'}],
    notes: String
})

const locationSchema = new Schema({
    name: String,
    description: String,
    owner: String,
    encounters: [{type: Schema.Types.ObjectId, ref: 'Encounter'}],
    notes: String
})

const characterSchema = new Schema({
    name: String,
    race: String,
    class: String,
    description: String,
    notes: String
})

const questSchema = new Schema({
    goal: String,
    reward: String,
    assignee: Schema.Types.Mixed,
    notes: String
})

const plotSchema = new Schema({
    //userID: {type: Schema.Types.ObjectId, ref: 'User'},
    userID: String,
    name: String,
    storyArcs: Number,
    countries: [{type: Schema.Types.ObjectId, ref: 'Country'}],
    quests: [{type: Schema.Types.ObjectId, ref: 'Quest'}],
    characters: [{type: Schema.Types.ObjectId, ref: 'Character'}],
    world: String,
    year: String,
    goal: String,
    laconic: String
})

plotSchema.post('remove', removePlot)
characterSchema.post('remove', removeCharacter)
locationSchema.post('remove', removeLocation)
shopSchema.post('remove', removeShop)
citySchema.post('remove', removeCity)
countrySchema.post('remove', removeCountry)
encounterSchema.post('remove', removeEncounter)
questSchema.post('remove', removeQuest)
encounterSchema.post('remove', removeEncounter)
citySchema.post('remove', removeCity)

const PlotModel = mongoose.model('Plot', plotSchema)
const CharacterModel = mongoose.model('Character', characterSchema)
const LocationModel = mongoose.model('Location', locationSchema)
const ShopModel = mongoose.model('Shop', shopSchema)
const CityModel = mongoose.model('City', citySchema)
const CountryModel = mongoose.model('Country', countrySchema)
const EncounterModel = mongoose.model('Encounter', encounterSchema)
const QuestModel = mongoose.model('Quest', questSchema)

module.exports = {
    Plot: PlotModel,
    Character: CharacterModel,
    Location: LocationModel,
    Shop: ShopModel,
    City: CityModel,
    Country: CountryModel,
    Encounter: EncounterModel,
    Quest: QuestModel
}

function removeReferences(doc, model, reference) {
    const query = {[reference]: {$in: doc._id}}
    
    model.find(query, function (err, res) {
        if (res !== undefined && res.length !== 0) {

            // Removes the encounter id from the array, while keeping any others
            var arrayList = res[0][reference].filter( function(value, index, arr) {
                if (value.toString() !== doc._id.toString()) {
                    return value
                }
            })

            res[0][reference] = arrayList

            // Updates the location with the new set of encounters
            const update = {$set: {[reference]: arrayList}}
            model.findOneAndUpdate(query, update).exec()
        }

    })

}

/**
 * Removes any of the child elements of the plot
 * @param {*} doc 
 */
function removePlot(doc) {

    // Removes the countries in the plot
    findAndRemove(CountryModel, 'countries', doc)

    // Removes the quests in the plot
    findAndRemove(QuestModel, 'quests', doc)

    // Removes the characters in the plot
    findAndRemove(CharacterModel, 'characters', doc)

}

function findAndRemove(model, attribute, doc) {
    model.find({_id: {$in: doc[attribute]}}, function (err, docs) {
        for (var i = 0; i < docs.length; i++) {
            model.findOne({_id: {$in: docs[i]._id}}, function (err, doc) {
                if (doc !== null)
                    doc.remove()
            })
        }
    }).exec()
}

/**
 * Remove character references from shops, encounters, and quests
 * @param {*} doc 
 */
function removeCharacter(doc) {
    removeReferences(doc, ShopModel, 'owner')
    removeReferences(doc, QuestModel, 'assignee')
    removeReferences(doc, EncounterModel, 'name')
}

/**
 * Remove location references from countries when a location is removed and removes child elements of the location
 * @param {*} doc 
 */
function removeLocation(doc) {
    // Removes any references to the location in the owning country
    removeReferences(doc, CountryModel, 'locations')

    // Removes the encounters that were in the location
    findAndRemove(EncounterModel, 'encounters', doc)
}

/**
 * Remove shop references from cities when a shop is removed
 * @param {*} doc 
 */
function removeShop(doc) {
    removeReferences(doc, CityModel, 'shops')
}

/**
 * Remove city references from countries when a city is removed and removes child elements of city
 * @param {*} doc 
 */
function removeCity(doc) {
    // Removes any references to the city in the owning country
    removeReferences(doc, CountryModel, 'cities')

    // Removes the shops that were in the city.
    ShopModel.deleteMany({_id: {$in: doc.shops}}).exec()
    findAndRemove(ShopModel, 'shops', doc)
}

/**
 * Removes any country references from the plot, and removes child elements of the country
 * @param {*} doc 
 */
function removeCountry(doc) {
    // Removes any references to the country in the owning plot
    removeReferences(doc, PlotModel, 'countries')

    // Removes the cities in the country
    findAndRemove(CityModel, 'cities', doc)

    // Removes the locations in the country
    findAndRemove(LocationModel, 'locations', doc)
}

/**
 * Remove encounter references from locations when an encounter is removed
 * @param {*} doc 
 */
function removeEncounter(doc) {
    removeReferences(doc, LocationModel, 'encounters')
}

/**
 * Remove quest references from the plot when a quest is removed
 * @param {*} doc 
 */
function removeQuest(doc) {
    removeReferences(doc, PlotModel, 'quests')
}
