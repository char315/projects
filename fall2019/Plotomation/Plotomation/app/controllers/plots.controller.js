const Models = require('../models/plots')
var mongoose = require('mongoose')
require('dotenv').config()
mongoose.Promise = global.Promise
mongoose.connect(process.env.DB_URL, {useNewUrlParser: true, useUnifiedTopology: true})

module.exports = {
    savePlot: savePlot,
    loadPlot: loadPlot,
    removeCity: removeCity,
    removeCountry: removeCountry,
    removeEncounter: removeEncounter,
    removeLocation: removeLocation,
    removeQuest: removeQuest,
    removeShop: removeShop
}


/**
 * Takes the plot object form the request and then forms the needed documents form the information provided.
 * @param {*} req Contains the plot object in req.body.plot
 * @param {*} res 
 */
function savePlot(req, res) {
    const plot = req
    var citiesJSON = []
    var countriesJSON = []
    var locationsJSON = []

    //
    //
    // Character Section.  Done first because it is referenced in later sections
    //  
    //

    // Storage for the character names and their IDs
    // Follows { NameOfCharacter: IDofCharacter }
    var characterIDs = {}

    if (plot.characters) {
        characterIDs = makeCharacters(plot.characters)
        plot.characters = Object.values(characterIDs)
    }


    //
    //
    // Country Section.  Takes care of cities, shops, locations, and their encounters
    //
    //

    if (plot.countries) {

        plot.countries.forEach(country => {
            // Resets the objects so that the previous countries' information is not used
            citiesJSON = []
            locationsJSON = []

            // Creates the cities and shops in the country if any exist
            if (JSON.stringify(country.cities) != '[{}]') {
                country.cities.forEach(city => {
                    if (JSON.stringify(city.shops) === '[{}]') {
                        city.shops = []
                        citiesJSON.push(city)
                        return;
                    }

                    shops = makeShops(city.shops, characterIDs)
                    city.shops = Object.values(shops)
                    citiesJSON.push(city)

                });
            }

            // Creates the locations and encounters in the country if any exist
            if (JSON.stringify(country.locations) != '[{}]') {
                country.locations.forEach(location => {
                    if (JSON.stringify(location.encounters) === '[{}]') {
                        location.encounters = []
                        locationsJSON.push(location)
                        return;
                    }
                    encounters = makeEncounters(location.encounters, characterIDs)
                    location.encounters = Object.values(encounters)
                    locationsJSON.push(location)
                })
            }

            var cityIDs = makeCities(citiesJSON)
            country.cities = Object.values(cityIDs)

            var locationIDs = makeLocations(locationsJSON)
            country.locations = Object.values(locationIDs)
            countriesJSON.push(country)

            
        });

        var countryIDs = makeCountries(countriesJSON);
        plot.countries = Object.values(countryIDs)
    }


    //
    //
    // Quest Section
    //
    //
    if (plot.quests) {
        var questIDs = makeQuests(plot.quests, characterIDs)
        plot.quests = Object.values(questIDs)
    }

    const submittedPlot = Models.Plot(plot)
    submittedPlot.save()
    return 'done'

}

/**
 * Grabs the information for the plot from the DB and sends back the full JSON object.
 * This will have a place in Sprint 3
 * @param {*} userId 
 * @param {*} plotName 
 */
function loadPlot(userId, plotName  ) {
    Models.Plot.find({userID: userId, name: plotName})
    .populate({
        path: 'countries',
        model: 'Country',
        populate: {
            path: 'cities',
            model: 'City',
            populate: {
                path:'shops',
                model: 'Shop'
            }
        },
    })
    .populate({
        path: 'quests',
        model: 'Quest',
    })
    .populate({
        path: 'locations',
        model: 'Location'
    })
    .populate({
        path: 'characters',
        model: 'Character'
    })
    .populate({
        path: 'countries',
        model: 'Country',
        populate: {
            path: 'locations',
            model: 'Location',
            populate: {
                path: 'encounters',
                model: 'Encounter'
            }
        }
    })
    .exec(function (error, results) {
        return(results)
    })
}

/**
 * This will be the function called from the routes to create a character
 * @param {*} character 
 */
function createCharacter(character) {
    makeCharacters(character)
}

/**
 * Function called to create an encounter
 * Creates the encounter and adds the references to the location
 * @param {*} encounter 
 * @param {*} location 
 */
function createEncounter(encounter, location) {

    var characterID = {}

    // Links a character to the encounter if there are any links
    Models.Character.find({name: encounter.name})
    .then(function (results) {
        characterID = {[encounter.name] : [results._id]}

        var encounters = []
        encounters.push(encounter)
        var encounterID = makeEncounters(encounters, characterID)

        //Gets the location and links the encounter to the location
    
        var oldLocation = {}
        Models.Location.find({_id: location._id})
        .then(function (results) {
            oldLocation = results;
            oldLocation.encounters.push(Object.values(encounterID))
            oldLocation.save()
        })
    })
}


//
//
//  Remove Element Functions
//
//

/**
 * Deletes a city from the DB.
 * @param {*} city 
 */
function removeCity(city) {
    removeElement(Models.City, city, 'name')
}

/**
 * Deletes a country from the DB.
 * @param {*} country 
 */
function removeCountry(country) {
    removeElement(Models.Country, country, 'name')
}

/**
 * Deletes an encounter from the DB.
 * @param {*} encounter 
 */
function removeEncounter(encounter) {
    removeElement(Models.Encounter, encounter, 'name')
}

/**
 * Deletes a location from the DB.
 * @param {*} location 
 */
function removeLocation(location) {
    removeElement(Models.Location, location, 'name')
}
/**
 * Deletes a city from the DB.
 * @param {*} plot 
 */
function removePlot(plot) {
    removeElement(Models.Plot, plot, 'name')
}

/**
 * Delets a quest from the DB
 * @param {*} quest 
 */
function removeQuest(quest) {
    removeQuest(Models.Quest, quest, 'goal')
}

/**
 * Deletes a shop from the DB
 * @param {*} shop 
 */
function removeShop(shop) {
    removeElement(Models.Shop, shop, 'name')
}

/**
 *  Helper Functions
 */

/**
 * Removes a specified document from the DB
 * @param {*} model The model of the document being removed
 * @param {*} element The contents of the document that are being search for
 * @param {*} attribute The attribute that the document will be searched for with.  Typically '_id' or 'name'
 */
function removeElement(model, element, attribute) {
    model.findOne({[attribute]: element[attribute]}, function (err, doc) {
        if (doc !== null) {
            doc.remove()
        }
    })
}

//
//
//  Element Making Functions
//
//

 /**
  * Creates the encounter documents and links any characters to them if available
  * @param {*} encounters 
  * @param {*} characters 
  */
function makeEncounters(encounters, characters) {
    var newEncounters = []
    encounters.map((encounter) => {
        if (characters[encounter.name.toString()]) {
            encounter.name = characters[encounter.name.toString()]
        }

        const e = new Models.Encounter(encounter)
        e.save();
        newEncounters.push(e._id)
    })
    return newEncounters
}

/**
 * Creates the character documents
 * @param {*} characters 
 */
function makeCharacters(characters) {
    var newCharacters = {}

    characters.map((character) => {
        const c = new Models.Character(character)
        c.save();
        newCharacters[c.name] = c._id
    })

    return newCharacters
}

/**
 * Creates the quest documents and links any characters if available
 * @param {*} quests 
 */
function makeQuests(quests, characters) {
    var newQuests = {}

    quests.map((quest) => {
        if (characters[quest.assignee.toString()]) {
            quest.assignee = characters[quest.assignee.toString()]
        }
        const q = new Models.Quest(quest)
        q.save();
        newQuests[q.goal] = q._id
    })

    return newQuests

}

/**
 * Creates the location documents
 * @param {*} locations 
 */
function makeLocations(locations) {
    var newLocations = {}
    locations.map((location) => {
        const l = new Models.Location(location)
        l.save();
        newLocations[l.name] = l._id
    })

    return newLocations

}

/**
 * Creates the country documents
 * @param {*} countries 
 */
function makeCountries(countries) {

    var newCountries = {}

    countries.map((country) => {
        const c = new Models.Country(country)
        c.save();
        newCountries[c.name] = c._id
    })

    return newCountries

}

/**
 * Creates the shop documents and links any characters if available
 * @param {*} shops 
 * @param {*} characters 
 */
function makeShops(shops, characters) {

    var newShops = {}

    shops.map((shop) => {
        if (characters[shop.owner.toString()]) {
            shop.owner = characters[shop.owner.toString()]
        }
        const s = new Models.Shop(shop)
        s.save();
        newShops[s.name] = s._id
    })

    return newShops;

}

/**
 * Creates the city documents
 * @param {*} cities 
 */
function makeCities(cities) {
    var newCities = {}

    cities.map((city) => {
        const c = new Models.City(city)
        c.save();
        newCities[c.name] = c._id
    })

    return newCities

}