const Models = require('../models/attributes')
var mongoose = require('mongoose')
const {db_url} = require('..//..//config.js');
require('dotenv').config()

mongoose.Promise = global.Promise
mongoose.connect(db_url, {useNewUrlParser: true, useUnifiedTopology: true})

module.exports = {
    getRandomAttribute: getRandomAttribute
}

/**
 * Gets a random attribute of the requested type.
 * @param {*} req 
 * @param {*} res 
 */
function getRandomAttribute(req, res) {

    var model;
    var query = {}
    var attribute
    var chosenValue = Math.random() < 0.5 ? 'Character' : 'Name'
    switch (Object.keys(req)[0]) {
        case 'sTitle':
            // Title
            model = Models.Term
            query = {
                Type: 'Title'
            }
            attribute = 'Name'
            break;
        case 'qAssignee':
            // Assignee
            model = Models.Term
            query = {
                Type: chosenValue
            }
            attribute = 'Name'
            break;
        case 'qReward':
            // Reward
            model = Models.Term
            query = {
                Type: 'Reward'
            }
            attribute = 'Name'
            break;

        case 'qGoal': 
        case 'tOverGoal':
        case 'eName':
            // Complication
            model = Models.Theme;
            query = {}
            attribute = 'Type'
            break;

        case 'tLaconic':
            // Complication - Laconic
            model = Models.Theme;
            query = {}
            attribute = 'Description'
            break;

        case 'cName':
            // Name
            model = Models.Term
            query = {
                Type: 'Name'
            }
            attribute = 'Name'
            break;

        case 'cDescription':
            // Description
            model = Models.Term
            query = {
                Type: 'Feature'
            }
            attribute = 'Name'
            break;

        case 'cRace':
            // Race
            model = Models.Term
            query = {
                Type: 'Race'
            }
            attribute = 'Name'
            break;

        case 'cClass':
            // Class
            model = Models.Term
            query = {
                Type: 'Class'
            }
            attribute = 'Name'
            break;

        case 'cPersonality':
            // Personality
            model = Models.Term
            query = {
                Type: 'Adjective'
            }
            attribute = 'Name'
            break;

        case 'lName':
        case 'tWorld':
            // Name
            model = Models.Term
            query = {
                Type: 'Location'
            }
            attribute = 'Name'
            break;
        
        case 'lDescriptionCO':
            // Country location
            model = Models.Term
            query = {
                Type: 'Country'
            }
            attribute = 'Name'
            break;
        case 'lDescriptionCI':
            // City location
            model = Models.Term
            query = {
                Type: 'City'
            }
            attribute = 'Name'
            break;
        case 'lDescriptionS':
            // Shop location
            model = Models.Term
            query = {
                Type: 'Shop'
            }
            attribute = 'Name'
            break;
        case 'lDescriptionG':
            // General location
            model = Models.Term
            query = {
                Type: 'Setting'
            }
            attribute = 'Name'
            break;
            
        case 'lOwner':
            // Owner
            model = Models.Term
            query = {
                Type: chosenValue
            }
            attribute = 'Name'
            break;
    }
    
    model.countDocuments(query).exec(function (err, count) {
        var random = Math.floor(Math.random() * count)

        model.findOne(query).skip(random).exec( function(err, result) {
            var stringresult = JSON.stringify(result)

            var jsonResult = JSON.parse(stringresult)

            res.status(200).send(jsonResult[attribute])
        })
    })

}