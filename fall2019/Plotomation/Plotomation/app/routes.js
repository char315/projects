const express = require('express'),
    router = express.Router(),
    plotController = require('./controllers/plots.controller')

module.exports = router;


router.post('/submitPost', plotController.savePlot)