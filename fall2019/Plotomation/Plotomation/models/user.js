// Require statements
var mongoose = require('mongoose');
var bcrypt = require('bcrypt');

/* 
 **** Defines DB Schema ****
 > Email - Data Type : String 
 > Username - Data Type : String
 > Password - Data Type : String
*/
var UserSchema = new mongoose.Schema({
  email: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  username: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  password: {
    type: String,
    required: true,
  },
  resetPwdToken: String,
  resetPwdExpiry: Date
});

/* 
 **** Password Authentication ****
 > Passwords gets hashed 
 > Compares with hash value in db
*/
UserSchema.statics.authenticate = function (email, password, callback) {
  User.findOne({ email: email })
    .exec(function (err, user) {
      if (err) {
        return callback(err)
      } else if (!user) {
        var err = new Error('User not found.');
        err.status = 401;
        return callback(err);
      }
      bcrypt.compare(password, user.password, function (err, result) {
        if (result === true) {
          return callback(null, user);
        } else {
          return callback();
        }
      })
    });
}

/* 
 **** Password Hash ****
 > Passwords gets hashed 
 > Hashed Password saved into DB
*/
UserSchema.pre('save', function (next) {
  var user = this;
  bcrypt.hash(user.password, 10, function (err, hash) {
    if (err) {
      return next(err);
    }
    user.password = hash;
    next();
  })
});

// Creates and exports User model
var User = mongoose.model('User', UserSchema);
module.exports = User;

