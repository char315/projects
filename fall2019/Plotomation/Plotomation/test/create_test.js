const assert = require('assert');
const PlotTest = require('../test_src/plot_test'); //imports the Plot model.
describe('Creating a new plot', () => {
    it('Creates a plot with userID: rajaa02', (done) => {
      
        const plot = new PlotTest({ userID: 'rajaa02', name:'Arul', storyArcs: 3, world:'Koalqp',year:2085,goal:'Success'});
        plot.save() 
            .then(() => {
                assert(!plot.isNew);
                done();
            })
    });
});