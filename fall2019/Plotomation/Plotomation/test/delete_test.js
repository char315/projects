const assert = require('assert');
const PlotTest = require('../test_src/plot_test'); //imports the Plot model.

describe('Deleting a plot', () => {

  beforeEach((done) => {
    plot = new PlotTest({ userID: 'terry1', name:'Terry', storyArcs: 5, world:'Koalqp',year:2085,goal:'Revenge'});
    plot.save()
      .then(() => done());
  });

  let plot;
  it('Deletes a plot with userID: terry1', (done) => {
    plot.remove()
      .then(() => PlotTest.findOne({ userID: 'terry1' }))
      .then((plot) => {
        assert(plot === null);
        done();
      });
  });

})
