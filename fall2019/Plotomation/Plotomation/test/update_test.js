const assert = require('assert');
const PlotTest = require('../test_src/plot_test'); //imports the Plot model.

describe('Updating a plot', () => {

    let plot;
  
    beforeEach((done) => {
      plot = new PlotTest({ userID: 'rajaa02', name:'Arul', storyArcs: 3, world:'Vulcan',year:2085,goal:'Victory'});
      plot.save()
        .then(() => done());
    });
    
    let rand = Math.floor(Math.random()*100) + 1;
    let ryear = Math.floor(Math.random()*5000) + 2000;

    function finder(statement, done) {
      statement
       .then(() => PlotTest.find({}))
       .then((testplots) => {
        assert(testplots[1].world === 'Tiburon'+rand)
        assert(testplots[1].year === String(ryear))
        done();
      });
    }

    it('Updates multiple fields in a plot', (done) => {
        //updates multiple fields
        finder(plot.update({world:'Tiburon'+rand, year:ryear}), done);
      });

    });