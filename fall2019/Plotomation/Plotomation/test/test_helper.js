const mongoose = require('mongoose');
const db_url  = "mongodb+srv://Admin:Admin@plotomation-0y18j.azure.mongodb.net/Plotomation?retryWrites=true&w=majority";

mongoose.Promise = global.Promise;
mongoose.connect(db_url, { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.connection
    .once('open', () => console.log('Connected!'))
    .on('error', (error) => {
        console.warn('Error : ',error);
    });

beforeEach((done) => {
    mongoose.connection.collections.testplots.drop(() => {
       
        done();
    }); 
});