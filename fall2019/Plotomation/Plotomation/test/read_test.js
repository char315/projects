const assert = require('assert');
const PlotTest = require('../test_src/plot_test'); //imports the Plot model.

let plot;

beforeEach(() => {
    plot = new PlotTest({ userID: 'bob15', name:'Bob', storyArcs: 5, world:'Orizon',year:1997,goal:'Victory'});
    plot.save()
        .then(() => done());
});
describe('Reading plot details', () => {
    it('Reads a plot with userID: bob15', (done) => {
        PlotTest.findOne({ name: 'bob15' })
            .then((pokemon) => {
                assert(plot.userID === 'bob15');
                done();
            })
            .catch((err)=> {
                console.log("Error: " + err);
            })   
    })
})