// Require statements
var express = require('express');
var app = express();
var ejs = require('ejs');
var path = require('path');
var flash = require('express-flash');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
const { db_url, secretKey, port} = require('./config');

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.static(__dirname + 'views/LoginPage/js'));
app.use(flash())

// Connect to MongoDB via Mongoose
mongoose.set('useCreateIndex', true);
mongoose.connect(db_url, { useNewUrlParser: true, useUnifiedTopology: true });
var db = mongoose.connection;

// MongoDB Error Handling
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  // Connection success!
});

// Use sessions for tracking logins
app.use(session({
  secret: secretKey,
  resave: true,
  saveUninitialized: false,
  store: new MongoStore({
    mongooseConnection: db
  })
}));

// Parses requests using express
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Serve static files from webpage directory
app.use(express.static(__dirname + '/LoginPage'));
app.use(express.static(__dirname + '/Main'));
app.use(express.static(__dirname + '/public'));

// Include routes
var routes = require('./server/route');
app.use('/', routes);

// Handles 404 error
app.use(function (req, res, next) {
  var err = new Error('File Not Found');
  err.status = 404;
  next(err);
});

// Error handler
// Define as the last app.use callback
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.send(err.message);
});

// Declaration of express working on chosen port
app.listen(port, function () {
  console.log('Express app listening on port ' + port);
});