// Require statements
var express = require('express');
var path = require('path');
var route = express.Router();
var User = require('../models/user');
var plotController = require('../app/controllers/plots.controller')
var attributeController = require('../app/controllers/attributes.controller')


var flash = require('express-flash');
var crypto = require('crypto');
var async = require('async');
var nodemailer = require('nodemailer');
const { emailprvdr, emailuname, emailpwd} = require('../config');

// GET request for reading data
route.get('/', function (req, res, next) {
  // Displays ejs login page
  res.render('LoginPage/index');
});

// POST request for updating data
route.post('/', function (req, res, next) {
  // Confirm that user typed same password twice
  if (req.body.password !== req.body.passwordConf) {
    var err = new Error('Passwords do not match.');
    err.status = 400;
    res.send("passwords dont match");
    return next(err);
  }

  if (req.body.email &&
    req.body.username &&
    req.body.password &&
    req.body.passwordConf) {

    // User is creating a new account; store new data
    var userData = {
      email: req.body.email,
      username: req.body.username,
      password: req.body.password,
    }

    User.create(userData, function (error, user) {
      if (error) {
        return next(error);
      } else {
        req.session.userId = user._id;
        return res.redirect('/profile');
      }
    });

  } else if (req.body.logemail && req.body.logpassword) {
    // User is logging in with existing credentials
    User.authenticate(req.body.logemail, req.body.logpassword, function (error, user) {
      if (error || !user) {
        var err = new Error('Wrong email or password.');
        err.status = 401;
        return next(err);
      } else {
        req.session.userId = user._id;
        return res.redirect('/profile');
      }
    });
  } else {
    // User has incomplete content in fields
    var err = new Error('All fields required.');
    err.status = 400;
    return next(err);
  }
})

var get_user = null;
// GET route after registering
route.get('/profile', function (req, res, next) {
  // Confirms registration result in html; otherwise displays unauthorized use of command
  User.findById(req.session.userId)
    .exec(function (error, user) {
      if (error) {
        return next(error);
      } else {
        if (user === null) {
          var err = new Error('Not authorized! Go back!');
          err.status = 400;
          return next(err);
        } else {
          //let reqPath = path.join(__dirname, '../');
          //return res.sendFile(reqPath + '/Main/main.html');
          get_user = user.username;
          res.render('MainPage/main',{username:get_user});
        }
      }
    });
});

//POST route for forgot password
route.post('/forgot',function (req,res, next)  {
  var token;

  //Runs below functions in series
  async.waterfall ([

    function(done) {
      crypto.randomBytes(20, function(err,buf){
      token = buf.toString('hex');
      done(err,token);
      });
    },

    //If user email found , set token and expiry
    function(token,done)
    {
      User.findOne({email: req.body.email}, function(err,user) {
        if(!user) {
          return res.send("No user found with the provided email address!");
        }
        user.resetPwdToken = token;
        user.resetPwdExpiry = Date.now() + 3600000;

        user.save(function(err) {
          done(err, token, user);
      });
    });
  },

  //Email setup using nodemailer
  function(token, user, done) {
    var smtpTransport = nodemailer.createTransport({
      service: emailprvdr,
      auth: {
        user: emailuname,
        pass: emailpwd
      }
    });

    var mailOptions = {
      to: user.email,
      from: 'support@plotomation.com',
      subject: 'Plotomation Password Reset',
      text: 'Hello ' + user.username + ',\n\n' + 
        'You are receiving this because you have requested the reset of the password for your account.\n\n' +
        'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
        'http://' + req.headers.host + '/reset/' + token + '\n\n' +
        'If you did not request this, please report security incident immediately.\n\n' +
        'Best Regards,\n\n' + 'Plotomation Accounts Team'
    };
    smtpTransport.sendMail(mailOptions, function(err) {
      res.send('An e-mail has been sent to ' + user.email + ' with further instructions');
      done(err, 'done');
    });
  }
  ],  function(err) {
        if (err) return next(err);
        res.redirect('/forgot');
      });
});

//GET route for password reset
route.get('/reset/:token', function(req, res) {
  User.findOne({ resetPwdToken: req.params.token, resetPwdExpiry: { $gt: Date.now() } }, function(err, user) {
    if (!user) {
      return res.send('Password reset token is invalid or expired');
    }
    res.render('LoginPage/forgot');
  });
});

//POST route for password reset
route.post('/reset/:token', function(req, res) {
  async.waterfall([
    function(done) {
      User.findOne({ resetPwdToken: req.params.token, resetPwdExpiry: { $gt: Date.now() } }, function(err, user) {
        if (!user) {
          return res.send('Password reset token is invalid or expired');
        }

        user.password = req.body.pswdConf;
        user.resetPwdToken = undefined;
        user.resetPwdExpiry = undefined;

        user.save(function(err) {
          done(err, user);
      });
      });
    },
    function(user, done) {
      var smtpTransport = nodemailer.createTransport({
        service: emailprvdr,
        auth: {
          user: emailuname,
          pass: emailpwd
        }
      });
      var mailOptions = {
        to: user.email,
        from: 'support@plotomation.com',
        subject: 'Your password has been changed',
        text: 'Hello ' + user.username + ',\n\n' +
          'This is a confirmation that the password for your account ' + user.email + ' has just been changed.\n\n' +
          'Best Regards,\n\n' + 'Plotomation Accounts Team'
      };
      smtpTransport.sendMail(mailOptions, function(err) {
        res.send('Success! Your password has been changed');
        done(err);
      });
    }
  ], function(err) {
    res.redirect('/');
  });
});


route.get('/changePwd',function(req,res,next) {
  res.render('LoginPage/forgot',{message:''});
});

route.post('/changePwd',function(req,res,next) {
    User.findOne({username: get_user}, function(err, user) {
      if (!user) {
        return res.send('You are not logged in!');
      }
      user.save(user.password = req.body.pswdConf);
    });
    res.render('LoginPage/forgot',{message:'Password changed!'});
});

// GET for logout
route.get('/logout', function (req, res, next) {
  if (req.session) {
    // Wipes session object
    req.session.destroy(function (err) {
      if (err) {
        return next(err);
      } else {
        return res.redirect('/');
      }
    });
  }
});
var plotJSON = null;
route.post('/api/submit', function (req, res, next) {

  plot = Object.keys(req.body)
  plotController.savePlot(JSON.parse(plot[0]).plot)
  plotJSON = JSON.parse(plot[0])
})

route.get('/api/random', function (req, res, next) {
    return attributeController.getRandomAttribute(req.query, res)
})

route.get('/plot', function(req,res,next) {
  res.render('PlotPage/plot',{plot_name:plotJSON.plot["name"],storyArcs:plotJSON.plot["storyArcs"],world:plotJSON.plot["world"],
  year:plotJSON.plot["year"],goal:plotJSON.plot["goal"],quests:plotJSON.plot["quests"],characters:plotJSON.plot["characters"],
  countries:plotJSON.plot["countries"],quests:plotJSON.plot["quests"]
  });
  })


module.exports = route;