const req = {
    body: {
        plot: {
            countries: [{
                name: 'Kalamon',
                description: 'Large southern country that is dominated by Humans and Elves',
                cities: [{
                    name: 'Allurmen',
                    city: 'Magical city',
                    shops: [{
                        name: 'Invulnerable Vagrant',
                        type: 'Magical General Goods',
                        owner: 'Magdur',
                        notes: 'Good relations with the party'
                    }, 
                    {
                        name: 'Hard Knocks',
                        type: 'Blacksmith',
                        owner: 'Krux',
                        notes: 'Doesn\t like Aelen since he was rude during one of their first transactions.'
                    }]
                },
                {
                    name: 'Lepidella',
                    description: 'City of mushroom people',
                    shops: [{
                        name: 'Crinkled Skin',
                        type: 'Leatherworking',
                        owner: 'Jerry',
                        notes: ''
                    }]
                }],
                locations:[{
                    name: 'Barbed Fields',
                    type: 'Area',
                    country: 'Jaitun',
    
                }],
                notes: 'This county is a lot more free in how they view magic'
            },
            {
                name: 'Gordal',
                description: 'Medium sized northern country that is ruled by strength of an individual.  Main races include monsters.  Goblins, orcs, etc.',
                cities: [{
                    name: 'Orcshire',
                    description: 'Border city on Gordal side',
                    shops: [{
                        name: 'Scarlet Flask',
                        type: 'Inn',
                        owner: 'Mera',
                        notes: ''
                    },
                    {
                        name: 'Devout Dealer',
                        type: 'Cleric Shop',
                        owner: 'Almar',
                        notes: ''
                    }]
                }]
            }],
            characters: [{
                name: 'Julie',
                race: 'Tiefling',
                class: 'Cleric',
                description: 'Tall, purple skin, kind',
                notes: 'Friends with party'
            }, 
            {
                name: 'Krux',
                race: 'Kobold',
                class: 'Cleric',
                description: 'Short, black scales, loyal',
                notes: 'Doesn\'t think things through'
            },
            {
                name: 'Magdur',
                race: 'Dwarf',
                class: 'Sorcerer',
                description: 'Strong, terrifying',
                notes: 'Begrudgingly helped party'
            },
            {
                name: 'Mera',
                race: 'Goliath',
                class: 'Paladin',
                description: 'Tall, strong',
                notes: 'Friends with the party'
            },
            {
                name: 'Jerry',
                race: 'Half-Orc',
                class: 'Paladin',
                description: 'Kind',
                notes: 'Protector of Lepidella'
            },
            {
                name: 'Almar',
                race: 'Gnome',
                class: 'None',
                description: 'Frugal',
                notes: 'Guide'
            }],
        }
    }
}